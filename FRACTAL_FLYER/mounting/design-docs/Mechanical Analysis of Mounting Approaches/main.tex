\documentclass{article}
\usepackage{array}
\usepackage{indentfirst}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{float}
\usepackage{caption}
\usepackage{sectsty}
\subsubsectionfont{\fontsize{12}{15}\selectfont}
\subsubsectionfont{\fontsize{11}{15}\selectfont}

\input{structure.tex} % Include the file specifying the document structure and custom commands

%----------------------------------------------------------------------------------------
%	ASSIGNMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Mechanical Analysis of Mounting Approaches} % Title of the assignment

\author{Dea Dressel \\
{\tt\small ddressel@stanford.edu}
\and
Ava Jih-Schiff\\
{\tt\small avaj@stanford.edu}} % Author name and email address


\date{Stanford University --- \today} % University, school and/or department name(s) and a date

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\section*{Introduction} % Unnumbered section

This document provides insight into how each mounting approach addresses a specific set of design and functional requirements, as well as a mechanical analysis for each mounting approach. Section 1 outlines the identified design and functional requirements for the mounting mechanisms. Section 2 discusses the proposed ceiling mount, Section 3 discusses the proposed staircase mount, and Section 4 discusses the proposed front window mount.


%----------------------------------------------------------------------------------------
%	Section 1
%----------------------------------------------------------------------------------------

\section{Requirements}

We have identified 12 total requirements for the mounting mechanism designs \footnote{Refer to "Design Review of Mounting Approaches" by Dressel and Jih-Schiff for detailed explanations of these requirements. }:

\begin{description}\begin{enumerate}
  \item Multiple Mounting Locations
  \item Material and Color
  \item Easy Installation and Removal of Mounting Mechanism
  \item Easy Attachment and Removal of Fractal Flyer 
  \item Adjustable
  \item Horizontal Extension
  \item Vertical Extension
  \item Weight Bearing
  \item Cost
  \item Earthquake Safety
  \item Ethernet Cable Connection
  \item Corrosion Protection
  
\end{enumerate}\end{description}

\begin{description}To satisfy the first requirement, this document offers mounting approaches for three different mounting locations: ceiling, staircase, and front window.\end{description}

%----------------------------------------------------------------------------------------
%	Section 2
%----------------------------------------------------------------------------------------

\section{Ceiling Mount}

The first mounting location is the ceiling. We will be hanging fractal flyers from the circular, steel shafts that extend across the stairwell ceiling. The current plan has 12 fractal flyers hanging from this location.

%------------------------------------------------

\subsection{Design}

See Figure 1.

\begin{figure}[H]
    \includegraphics[width=0.9\textwidth]{Ceiling-Option.png}
    \caption{Ceiling Mounting Design}
\end{figure}

%------------------------------------------------

\subsection{Requirements}

This subsection details how the above mounting approach for the ceiling addresses the 12 requirements laid out in Section 1.\\

\noindent\textbf{Material and Color:} The wire cable material of this approach keeps to the industrial feel and coloring of the building.\\\\
This mounting approach requires the following materials:

\begin{description}\begin{itemize}
    \item 2.5 ft of $\frac{1}{8}$", 7x19, Vinyl Coated Galvanized Cable 
    \item Variable length of $\frac{1}{8}$", 7x19, Vinyl Coated Galvanized Cable
    \item Four $\frac{1}{8}$" Aluminum Sleeves 
    \item Four $\frac{1}{8}$" Light Duty Wire Rope Thimbles
    \item Two $\frac{3}{16}$" Electro Galvanized Spring Snap Links (aka Carabiner)
\end{itemize}\end{description}
\\
\textbf{Easy Installation and Removal of Mounting Mechanism:} Installing and removing this design is a two person job that is estimated to take around five minutes. It does not require any special tools other than scaffolding to reach the locations. This design does not require any permanent changes to the building components.\\\\
\textbf{Easy Attachment and Removal of Fractal Flyer:} Attaching and removing a fractal flyer to this design is a two person job that is estimated to take one minute. We expect one person would need to lift the fractal flyer from below to loosen the connection of the turnbuckles with the carabiner, after which the other person would unclip the fractal flyer and lower it down. It does not require any special tools other than scaffolding to reach the locations. \\\\
\textbf{Adjustable:} When cutting and clamping the wire cable and attaching the two thimbles, the wire can be adjusted to be any length.\\\\
\textbf{Horizontal Extension:} The ceiling-mounted fractal flyers do not need any horizontal extension from the mounting location.\\\\
\textbf{Vertical Extension:} The vertical distance from mounting location is variable and can be adjusted.\\\\
\textbf{Weight Bearing:} As addressed below in the mechanical analysis, this design is rated for a 105 lb load, far exceeding the 10 lbs weight of the fractal flyer.\\\\
\textbf{Cost:} The total cost per mount of $\$3.68$ comes in far below the budget of $\$50.00$ per mount. Below are the calculated costs, based on standard parts, and assuming a total of 8ft of cable used. \\

 \begin{tabular}{ll}
     \textbf{Cable (8ft):}  & $\$0.20$ x 8\\ 
     \textbf{Crimping Sleeves:}  & $\$0.10$ x 4\\ 
     \textbf{Thimble:}  & $\$0.20$ x 4\\ 
     \textbf{Carabiner Clip:}  & $\$0.40$ x 2\\ 
     \rule{120}{1} & \rule{39}{1}\\ 
     \textbf{Total Cost per Mount:}  & $\$3.60$\\ 
 \end{tabular}
 \\\\\\
\textbf{Earthquake Considerations:} This mounting approach does not run a high risk of coming unattached from the building during an earthquake. Instead, the exact location of the mount may move along the circular ceiling shafts from extreme swinging of the fractal flyer during an earthquake. Since the circular shafts are connected directly to the building on both ends, the mounts won't be able to scoot all the way off of the shaft, eliminating the risk of the entire mount coming loose. Additionally, there should be a limitation on the length of the vertically extended wire cable so that the fractal flyer cannot swing into any surrounding walls, windows, or other flyers. Lastly, since the mounting components and fractal flyer together weigh under 20 lbs, the design does not need to meet the seismic design requirements of nonstructural building components within \textit{Minimum Design Loads and Associated Criteria for Buildings and Other Structures} (ASCE 7).\\\\
\textbf{Ethernet Cable Connection:} The Ethernet cable will be run from the headboard to each flyer along the shafts and down the wire loop. We will use zip ties spaced along the wire to secure it. \\\\
\textbf{Corrosion Protection:} The main consideration for corrosion is the interaction between the wire and the ceiling shaft from which it is hanging. To ensure that the wire retained it's integrity and to limit any potential damage or paint chips on the shaft, the cable chosen has a vinyl coating. Coated cable allows for great abrasion protection and flexibility. Additionally, the 7x19 configuration is also an excellent choice for flexibility and durability, reducing the chances of cable breakage or fraying. The second consideration for corrosion is the interaction between the various pieces on the mount. To address this, the thimbles and carabiner chosen are both made of the same zinc-plated steel. This material will not corrode itself and is safe to use with the vinyl coated cable. The aluminum crimping sleeves were chosen for their ability to effectively crimp 7x19 galvanized cable, and we will be cutting away the vinyl coating in the crimping location to allow for this interaction.\\\\

%------------------------------------------------

\subsection{Mechanical Analysis}

The ceiling mounting approach is simple in it's design. Therefore, we did not feel the need to make a CAD model. Instead, our analysis will be in demonstrating how the components of the mounting design are rated for a weight larger than that of the fractal flyer.\\

\textbf{Vinyl Coated Galvanized Cable: } This type of cable has a minimum breaking load of 2000 lbs and a working load of 400 lbs. This far exceeds the 10 lbs weight of the fractal flyer. 

% The vinyl coating adds a protective layer between the cable and the ceiling shaft to reduce paint chipping in the event of movement or swaying.

\textbf{Electro Galvanized Spring Snap Link: } This type of carabiner has a working load limit of 105 lbs. This far exceeds the 10lbs weight of the fractal flyer.

\textbf{Light Duty Wire Rope Thimble: } These thimbles act as a guard to protect the cable from excessive crimping, fraying and bending that could result in a point load at the bottom of a cable loop. Therefore, these pieces should have no problem maintaining the integrity of the cable in the presence of a vertical load.

\textbf{Aluminum Sleeve: } If properly swaged, this type of crimping sleeve has a termination efficiency of 90\%. Properly swaging the sleeves will require cutting back the vinyl coating of the wire where it is looped around the thimbles. Removing any of the vinyl coating lowers the working load to 360 lbs, which is still far over the weight of the fractal flyer. 

%----------------------------------------------------------------------------------------
%	Section 3
%----------------------------------------------------------------------------------------

\section{Staircase Mount}

The second mounting location is the staircase. We will be extending fractal flyers horizontally from the inner or outer edge of the staircase. The current plan has 39 fractal flyers hanging from this location: 35 on the exterior and 4 on the interior.

%------------------------------------------------

\subsection{Design}

See Figure 2. 
\begin{figure}
    \includegraphics[width=0.9\textwidth]{Staircase-Design.png}
    \caption{Staircase Mounting Design}
\end{figure}

%------------------------------------------------

\subsection{Requirements}

This subsection details how the above mounting approach for the staircase addresses the 12 requirements laid out in Section 1.\\

\noindent\textbf{Material and Color:} All steel used in this design (clamps, threaded rods, pipe hangers, and pipe) will be painted to color match the painted steel in the stairwell. The wire cable used for the vertical extension of the fractal flyer similarly keeps to the industrial feel and coloring of the building.\\\\
This mounting approach requires the following materials:

\begin{description}\begin{itemize}
    \item Variable length of $\frac{1}{8}$", 7x19, Vinyl Coated Galvanized Cable
    \item Two $\frac{1}{8}$" Aluminum Sleeves 
    \item Two $\frac{1}{8}$" Light Duty Wire Rope Thimbles
    \item One $\frac{3}{16}$" Electro Galvanized Spring Snap Links (aka Carabiner)
    \item Three $\frac{1}{4}$" Threaded-Rod-Mount Clamping Hangers
    \item One $\frac{3}{4}$" Eyebolt
    \item Two Beam Clamp for $\frac{1}{4}$" Threaded Rod
    \item Two $\frac{1}{4}$" Threaded Rod, 4" length
    \item Variable length of $\frac{1}{2}$" Schedule 40 Steel Pipe
    \item 12 $\frac{1}{4}$" Steel Hex Nuts
\end{itemize}\end{description}
\\
\textbf{Easy Installation and Removal of Mounting Mechanism:} Installing and removing this design is a two person job that is estimated to take around ten minutes. It does not require any special tools other than scaffolding to reach the locations and a wrench for tightening the clamps. This design does not require any permanent changes to the building components.\\\\
\textbf{Easy Attachment and Removal of Fractal Flyer:} Attaching and removing a fractal flyer to this design is a two person job that is estimated to take one minute. We expect one person would need to lift the fractal flyer from below to loosen the connection of the turnbuckles with the carabiner, after which the other person would unclip the fractal flyer and lower it down. It does not require any special tools other than scaffolding to reach the locations. \\\\
\textbf{Adjustable:} The exact length of the steel pipe can be chosen and cut to size. When cutting and clamping the wire cable, the wire can be adjusted to be any length.\\\\
\textbf{Horizontal Extension:} The steel pipe length can be adjusted to achieve the desired horizontal extension.\\\\
\textbf{Vertical Extension:} The wire cable length can be adjusted to achieve the desired vertical extension.\\\\
\textbf{Weight Bearing:} As long as the steel pipe length is 5 ft or shorter, the steel pipe will bend within a reasonable and safe range. This is discussed in detail within the Mechanical Analysis subsection below.\\\\ 
\textbf{Cost:} The total cost per mount of $\$37.81$ comes in below the budget of $\$50.00$ per mount. Below are the calculated costs, based on standard parts and assuming a maximum cable length of 8 ft. We anticipate that the cost of the steel pipe can be lowered if we purchase bulk quantities of 1/2" schedule 40 pipe and cut to the desired length, rather than purchasing measured, pre-cut sections.\\

 \begin{tabular}{ll}
     \textbf{Cable (8ft):}  & $\$0.20$ x 8\\ 
     \textbf{Crimping Sleeves:}  & $\$0.10$ x 2\\ 
     \textbf{Thimble:}  & $\$0.20$ x 2\\ 
     \textbf{Carabiner Clip:}  & $\$0.40$ x 1\\
     \textbf{Hanger with Closure Bolt and Nut:}  & $\$1.14$ x 3\\
     \textbf{Eyebolt:} & $\$3.20$ x 1\\
     \textbf{Beam Clamp:}  & $\$2.65$ x 2\\ 
     \textbf{4" Threaded Rod:}  & $\$0.52$ x 2\\
     \textbf{Steel Pipe (3ft):}  & $\$2.63$ x 3\\
     \textbf{Hex Nuts:}  & $\$0.08$ x 12\\
     \rule{120}{1} & \rule{39}{1}\\ 
     \textbf{Total Cost:}  & $\$24.41$\\ 
 \end{tabular}
 \\\\\\
\textbf{Earthquake Considerations:}  Since this mounting design has two c-clamps on either side of the I-beam, neither clamp can walk-off of the beam, eliminating the risk of the entire mount coming loose. In the case of extreme shaking during an earthquake, the clamps may slightly loosen which could allow the mount to slide down the staircase I-beam. However, this would require intense shaking for an extended period of time. Additionally, there should be a limitation on the length of the vertically extended wire cable so that the fractal flyer cannot swing into any surrounding walls, windows, or other flyers. Lastly, since the mounting components and fractal flyer together weigh under 20 lbs, the design does not need to meet the seismic design requirements of nonstructural building components within \textit{Minimum Design Loads and Associated Criteria for Buildings and Other Structures} (ASCE 7).\\\\
\textbf{Ethernet Cable Connection:} The Ethernet cable will be run from the headboard to each flyer along the staircase, across the steel pipe, and down the wire cable. We will use zip ties spaced along the shafts, pipe, and wire to secure it. \\\\
\textbf{Corrosion Protection:} The staircase I-beams are painted which provides considerable corrosion protection between the I-beams and the beam clamps. The pipe, pipe hangers, hex nuts, threaded rods and c-clamps are all metals that do not corrode when in contact. Refer to the Corrosion Protection portion of Subsection 2.2 for details on the wire cable component of this mounting approach. 

%------------------------------------------------

\subsection{Mechanical Analysis}

%----------------------------------
\subsubsection{Full Model}

Below is a SolidWorks model of the staircase mounting approach.

\begin{center}
    \includegraphics[width=0.8\textwidth]{Staircase-Solidworks-Full-2.png}\\
\end{center}

\begin{center}
    \includegraphics[width=0.8\textwidth]{Staircase-Solidworks-Full-3.png}\\
\end{center}
\\\\
SolidWorks ran into issues with meshing the threads on the clamps, threaded rods, hex nuts and eye bolt, each part CAD downloaded directly from McMaster-Carr. In order to perform a finite element analysis of a fractal flyer load on the far end of the pipe, the model had to be simplified to exclude any threaded components.

%----------------------------------
\subsubsection{Simplified Model}

Simplifying the model required identifying the elements that would be the first to fail under the load of the fractal flyer. The beam clamps in this approach are rated for a maximum load of 100 lbs; Given the 10 lbs weight of a fractal flyer, the beam clamps are not an area for concern. The threaded rods have a tensile strength of 50,000 psi. The hex nuts will remain in place if tightened correctly. Therefore, the simplified model became the pipe with three pipe hangers, two which are connected to the beam clamps and one with the eye bolt which connects to the wire that holds the fractal flyer.

\begin{center}
    \includegraphics[width=0.8\textwidth]{Staircase-Solidworks-Simplified.png}\\
\end{center}

The first pipe hanger (starting from the left) is placed 1" in from the edge of the pipe. The second pipe hanger is 7.1" away from the first pipe hanger, modeling the flange width of the staircase I-beam. The last pipe hanger is 2" in from the edge of the pipe. While the length of the pipe changes, these pipe hanger distances remain constant.

The following finite element analysis was performed using the SolidWorks' simulation tools. The top faces of the two pipe hangers attached to the beam clamps were set as fixed geometries. Gravity was set to act downward on the mounting mechanism. A 45 Newton force representing the fractal flyer was set on the flat face of the pipe hanger attached to the eye bolt.  The 45 Newton number was calculated based on the weight of the fractal flyer($10 lbs \approx 4.5 kg$) and acceleration due to gravity($\approx$ 10 $m/s^{2}$).\\

\\
\begin{center}
    \includegraphics[width=0.8\textwidth]{Staircase-Solidworks-FEA.png}\\
\end{center}

%----------------------------------
\subsubsection{Results}

The simulation was run on 8 different mounting configurations that varied in pipe diameter and length. The results demonstrate how $\frac{1}{2}$" and $\frac{3}{4}$" pipes act in the presence of an end load at the selected 4 lengths: 2ft, 3ft, 5ft, and 10ft. Below are images comparing the thicknesses of the two varieties of schedule 40 steel pipe.\\

\begin{center}
    \includegraphics[width=0.4\textwidth]{Pipe-1-2-in.png}
    \includegraphics[width=0.4\textwidth]{Pipe-3-4-in.png}
\end{center}

After meshing, adding component interactions, adding external loads, and running the simulation, SolidWorks created displacement plots of each configuration. Below is an image of the displacement plot for the $\frac{1}{2}$" pipe at a length of 3ft.\\

\begin{center}
    \includegraphics[width=0.8\textwidth]{Staircase-1-2in-3ft-Displacement-Plot.png}
\end{center}

Below is a table with the maximum displacement observed at the end of the pipe for each configuration by size and length of pipe. \\

\begin{center}
\begin{tabular}{ |c|c|c| } 
\hline
\textbf{Pipe Size (in)} & \textbf{Pipe Length (ft)} & \textbf{Max Displacement (mm)} \\
\hline
\multirow{4}{4em}{1/2} & 2 & 0.39 \\ 
 & 3 & 2.85 \\ 
 & 5 & 22.4 \\ 
 & 10 & 130 \\ 
\hline
\multirow{4}{4em}{3/4} & 2 & 0.22 \\ 
 & 3 & 1.48 \\ 
 & 5 & 11.34 \\ 
 & 10 & 85.5 \\ 
\hline
\end{tabular}
\end{center}

\subsubsection{Analysis}

The 2ft, 3ft and 5ft pipe lengths for both the 1/2" and 3/4" pipes saw a maximum displacement under 1" (22.4 mm). This is a reasonable level of displacement that does not have any cause for concern. The 10ft length was included to demonstrate that even with an extreme pipe length, the pipe will have larger displacement but will not break. The results of this mechanical analysis prove that the mounting approach for the staircase location is sufficient. 

%----------------------------------------------------------------------------------------
%	Section 4
%----------------------------------------------------------------------------------------

\section{Window Mount}

The third mounting location is the front window. We will be extending fractal flyers outward from the horizontal T-beams. The current plan has 25 fractal flyers hanging from this location.

%------------------------------------------------

\subsection{Design}

See Figure 3.

\begin{figure}
    \includegraphics[width=0.9\textwidth]{Front-Window-Design.jpeg}
    \caption{Ceiling Mounting Design}
\end{figure}

%------------------------------------------------

\subsection{Requirements}

This subsection details how the above mounting approach for the staircase addresses the 12 requirements laid out in Section 1.\\

\noindent\textbf{Material and Color:} All steel used in this design (clamps, threaded rods, pipe hangers, and pipe) will be painted to color match the painted steel in the stairwell. The wire cable used for the vertical extension of the fractal flyer similarly keeps to the industrial feel and coloring of the building.\\\\
This mounting approach requires the following materials:

\begin{description}\begin{itemize}
    \item Variable length of $\frac{1}{8}$", 7x19, Vinyl Coated Galvanized Cable
    \item Two $\frac{1}{8}$" Aluminum Sleeves 
    \item Two $\frac{1}{8}$" Light Duty Wire Rope Thimbles
    \item One $\frac{3}{16}$" Electro Galvanized Spring Snap Links (aka Carabiner)
    \item Three $\frac{1}{4}$" Threaded-Rod-Mount Clamping Hangers
    \item One $\frac{3}{4}$" Eyebolt
    \item Two Beam Clamp for $\frac{1}{4}$" Threaded Rod
    \item One $\frac{1}{4}$" Threaded Rod, 2.5" length
    \item One $\frac{1}{4}$" Threaded Rod, 8" length
    \item Variable length of $\frac{1}{2}$" Schedule 40 Steel Pipe
    \item One Retaining Strap($\frac{1}{2}$" x $\frac{1}{8}$" Steel Flat Bar, length around 1')
    \item 12 $\frac{1}{4}$" Steel Hex Nuts
\end{itemize}\end{description}
\\
\textbf{Easy Installation and Removal of Mounting Mechanism:} Installing and removing this design is a two person job that is estimated to take around ten minutes. It does not require any special tools other than scaffolding to reach the locations and a wrench for tightening the clamps. This design does not require any permanent changes to the building components.\\\\
\textbf{Easy Attachment and Removal of Fractal Flyer:} Attaching and removing a fractal flyer to this design is a two person job that is estimated to take one minute. We expect one person would need to lift the fractal flyer from below to loosen the connection of the turnbuckles with the carabiner, after which the other person would unclip the fractal flyer and lower it down. It does not require any special tools other than scaffolding to reach the locations. \\\\
\textbf{Adjustable:} The exact length of the steel pipe can be chosen and cut to size. When cutting and clamping the wire cable, the wire can be adjusted to be any length.\\\\
\textbf{Horizontal Extension:} The steel pipe length can be adjusted to achieve the desired horizontal extension.\\\\
\textbf{Vertical Extension:} The wire cable length can be adjusted to achieve the desired vertical extension.\\\\
\textbf{Weight Bearing:} As long as the steel pipe length is 5 ft or shorter, the steel pipe will bend within a reasonable and safe range. This is discussed in detail within the Mechanical Analysis subsection below.\\\\ 
\textbf{Cost:} The total cost per mount of $\$13.28$ comes in below the budget of $\$50.00$ per mount. Below are the calculated costs, based on standard parts, and assuming 8ft of cable used. We anticipate that the cost of the steel pipe can be lowered if we purchase bulk quantities of schedule 40 pipe and cut to the desired length, rather than purchasing pre-cut and measured 3ft sections.\\

 \begin{tabular}{ll}
     \textbf{Cable (8ft):}  & $\$0.20$ x 8\\ 
     \textbf{Crimping Sleeves:}  & $\$0.10$ x 2\\ 
     \textbf{Thimble:}  & $\$0.20$ x 2\\ 
     \textbf{Carabiner Clip:}  & $\$0.40$ x 1\\
     \textbf{Hanger with Closure Bolt and Nut:}  & $\$1.14$ x 3\\
     \textbf{Eyebolt:} & $\$3.20$ x 1\\
     \textbf{Beam Clamp:}  & $\$2.65$ x 2\\ 
     \textbf{Threaded Rod (2.5" length):}  & $\$0.31$ x 1\\
     \textbf{Threaded Rod (8" length):}  & $\$0.97$ x 1\\
     \textbf{Steel Pipe (3ft):}  & $\$2.63$ x 3\\
     \textbf{Retaining Strap:}  & $\$0.50$ x 1\\
     \textbf{Hex Nuts:}  & $\$0.08$ x 12\\
     \rule{120}{1} & \rule{39}{1}\\ 
     \textbf{Total Cost:}  & $\$25.15$\\ 
 \end{tabular}
 \\\\\\
\textbf{Earthquake Considerations:} Since this mounting design has three connection points to the T-beam, all three connections cannot simultaneously come loose, eliminating the risk of the entire mount coming loose. Additionally, there should be a limitation on the length of the vertically extended wire cable based on the horizontal extension of the pipe so that the fractal flyer cannot swing into any surrounding walls, windows, or other flyers. Lastly, since the mounting components and fractal flyer together weigh under 20 lbs, the design does not need to meet the seismic design requirements of nonstructural building components within \textit{Minimum Design Loads and Associated Criteria for Buildings and Other Structures} (ASCE 7).\\\\
\textbf{Ethernet Cable Connection:} The Ethernet cable will be run from the headboard to each flyer along the shafts, along the steel pipe, and down the wire loop. We will use zip ties spaced along the shafts, pipe, and wire to secure it. \\\\
\textbf{Corrosion Protection:} The considerations made in section 3.2 for the staircase mount apply here as well. All metal parts, including clamps, etc., used for the staircase will be identical for the window mechanism.

%------------------------------------------------

\subsection{Mechanical Analysis}

%----------------------------------
\subsubsection{Full Model}
Below is a SolidWorks model of the front window mounting approach.

\begin{center}
    \includegraphics[width=0.8\textwidth]{Front-Window-Full-Model.png}
\end{center}
\begin{center}
    \includegraphics[width=0.8\textwidth]{Front-Window-Full-Model-Angle-View.png}
\end{center}
\\\\

Same as for the staircase mount model, SolidWorks ran into issues with meshing the threads on the clamps, threaded rods, hex nuts and eye bolt. In order to perform a finite element analysis of a fractal flyer load on the far end of the pipe, the model had to be simplified to exclude any threaded components.

%----------------------------------
\subsubsection{Simplified Model}

The reasoning behind the simplified front window mount model is the exact same as that for the simplified staircase mount model in Section 3.3.2. Therefore, the simplified mount model became the pipe with three pipe hangers, two which are connected to the beam clamps and one with the eye bolt which connects to the wire that holds the fractal flyer.


\begin{center}
    \includegraphics[width=0.8\textwidth]{Front-Window-Simplified-Model.png}\\
\end{center}
\\
\begin{center}
    \includegraphics[width=0.8\textwidth]{Front-Window-Simplified-Model-Simulation.png}\\
\end{center}

The first pipe hanger (starting from the left) is placed 1" in from the edge of the pipe. The second pipe hanger is 5" away from the first pipe hanger, modeling the web length of the front window T-beam. The last pipe hanger is 2" in from the edge of the pipe. While the length of the pipe changes, these pipe hanger distances remain constant.

The following finite element analysis was performed using the SolidWorks' simulation tools. The top faces of the two pipe hangers attached to the beam clamps were set as fixed geometries. Gravity was set to act downward on the mounting mechanism. A 45 Newton force representing the fractal flyer was set on the flat face of the pipe hanger attached to the eye bolt.\\

%----------------------------------
\subsubsection{Results}

The simulation was run on 4 different mounting configurations that varied in pipe length. The results demonstrate how $\frac{1}{2}$" pipe acts in the presence of an end load at the selected 4 lengths: 2ft, 3ft, 5ft, and 10ft. The decision to exclude the $\frac{3}{4}$" pipe in the front window simulation was because the maximum displacements seen for the staircase $\frac{1}{2}$" pipe were satisfactory, and $\frac{1}{2}$" pipe is cheaper. Therefore, we decided to go ahead with $\frac{1}{2}$" pipe. 

After meshing, adding component interactions, adding external loads, and running the simulation, SolidWorks created displacement plots of each configuration. Below is an image of the displacement plot for the $\frac{1}{2}$" pipe at a length of 3ft.\\\\

\begin{center}
    \includegraphics[width=0.8\textwidth]{Front-Window-1-2in-3ft-Displacement-Plot.png}
\end{center}

Below is a table with the maximum displacement observed at the end of the pipe by length of pipe. \\

\begin{center}
\begin{tabular}{ |c|c|c| } 
\hline
\textbf{Pipe Size (in)} & \textbf{Pipe Length (ft)} & \textbf{Max Displacement (mm)} \\
\hline
\multirow{4}{4em}{$\frac{1}{2}$} & 2 & 0.804 \\ 
 & 3 & 3.941 \\ 
 & 5 & 27.72 \\ 
 & 10 & 143.4 \\ 
\hline

\end{tabular}
\end{center}

\subsubsection{Analysis}

The 2ft and 3ft pipe lengths for the 1/2" pipe saw a maximum displacement under 1" (22.4 mm). The 5ft pipe length saw a maximum displacement of slightly more than 1". This is a reasonable level of displacement that does not have any cause for concern. The 10ft length was included to demonstrate that even with an extreme pipe length, the pipe will have larger displacement but will not break. The results of this mechanical analysis prove that the mounting approach for the front window location is sufficient. 


\end{document}
