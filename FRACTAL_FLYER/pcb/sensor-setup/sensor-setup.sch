EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "FLIGHT Fractal Flyer Motor/Sensor Test Configuration, EE125"
Date "2021-06-29"
Rev "1.0"
Comp "Stanford University"
Comment1 "The SAMD51 and the sensors are connected through jumper wires from a prototype board."
Comment2 "The sensors connected on J2-J4 are Sparkfun LIS3DH boards."
Comment3 "on a Fractal Flyer. The PWM lines go to a Cytron MDD3A H-bridge board."
Comment4 "This shows the schematic of the wiring hookup Phil uses for testing sensors and motors"
$EndDescr
Text Notes 2050 1000 0    79   ~ 0
THRU-HOLE HEADERS\n    (SAMD51 I/O)
$Comp
L tail-rescue:GND-power #PWR?
U 1 1 5E42AD98
P 1350 2700
AR Path="/5E41758F/5E42AD98" Ref="#PWR?"  Part="1" 
AR Path="/5E42AD98" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 1350 2450 50  0001 C CNN
F 1 "GND" V 1355 2572 50  0000 R CNN
F 2 "" H 1350 2700 50  0001 C CNN
F 3 "" H 1350 2700 50  0001 C CNN
	1    1350 2700
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:GND-power #PWR?
U 1 1 5E42AD9F
P 1350 1500
AR Path="/5E41758F/5E42AD9F" Ref="#PWR?"  Part="1" 
AR Path="/5E42AD9F" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 1350 1250 50  0001 C CNN
F 1 "GND" V 1355 1372 50  0000 R CNN
F 2 "" H 1350 1500 50  0001 C CNN
F 3 "" H 1350 1500 50  0001 C CNN
	1    1350 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	1450 2400 1950 2400
Wire Wire Line
	1450 2300 1950 2300
Wire Wire Line
	1450 2200 1950 2200
Wire Wire Line
	1250 1700 1950 1700
Text Label 1650 2400 0    50   ~ 0
MISO
Text Label 1650 2300 0    50   ~ 0
MOSI
Text Label 1650 2200 0    50   ~ 0
SCK
Wire Wire Line
	1950 1500 1350 1500
Wire Wire Line
	1950 2700 1350 2700
NoConn ~ 1950 2800
NoConn ~ 3500 1400
NoConn ~ 3500 1500
NoConn ~ 3500 1600
NoConn ~ 3500 1300
NoConn ~ 3500 2000
NoConn ~ 3500 2200
NoConn ~ 3500 2500
Text Label 1650 1200 0    50   ~ 0
RESET
$Comp
L power:GND #PWR0103
U 1 1 5E504DCC
P 3200 3850
F 0 "#PWR0103" H 3200 3600 50  0001 C CNN
F 1 "GND" V 3205 3722 50  0000 R CNN
F 2 "" H 3200 3850 50  0001 C CNN
F 3 "" H 3200 3850 50  0001 C CNN
	1    3200 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 3850 3550 3850
Text GLabel 3250 3750 0    50   Input ~ 0
+3V3
Wire Wire Line
	3250 3750 3550 3750
$Comp
L Connector_Generic:Conn_01x06 J4
U 1 1 5E558231
P 3750 4350
F 0 "J4" H 3830 4342 50  0000 L CNN
F 1 "Conn_01x06" H 3830 4251 50  0000 L CNN
F 2 "jst:JST_1x6" H 3750 4350 50  0001 C CNN
F 3 "~" H 3750 4350 50  0001 C CNN
F 4 "455-2271-ND" H 3750 4350 50  0001 C CNN "Digikey PN"
F 5 "B6B-XH-A(LF)(SN)" H 3750 4350 50  0001 C CNN "Manufacturer PN"
F 6 "JST Sales America Inc." H 3750 4350 50  0001 C CNN "Manufacturer"
F 7 "CONN HEADER VERT 6POS 2.5MM" H 3750 4350 50  0001 C CNN "Description"
	1    3750 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 4150 3250 4150
Wire Wire Line
	3550 4250 3250 4250
Wire Wire Line
	3550 4350 3250 4350
Wire Wire Line
	3550 4450 3250 4450
$Comp
L power:GND #PWR0104
U 1 1 5E558243
P 3200 4650
F 0 "#PWR0104" H 3200 4400 50  0001 C CNN
F 1 "GND" V 3205 4522 50  0000 R CNN
F 2 "" H 3200 4650 50  0001 C CNN
F 3 "" H 3200 4650 50  0001 C CNN
	1    3200 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 4650 3550 4650
Text GLabel 3250 4550 0    50   Input ~ 0
+3V3
Wire Wire Line
	3250 4550 3550 4550
$Comp
L Connector_Generic:Conn_01x06 J5
U 1 1 5E55F7DF
P 3750 5150
F 0 "J5" H 3830 5142 50  0000 L CNN
F 1 "Conn_01x06" H 3830 5051 50  0000 L CNN
F 2 "jst:JST_1x6" H 3750 5150 50  0001 C CNN
F 3 "~" H 3750 5150 50  0001 C CNN
F 4 "455-2271-ND" H 3750 5150 50  0001 C CNN "Digikey PN"
F 5 "B6B-XH-A(LF)(SN)" H 3750 5150 50  0001 C CNN "Manufacturer PN"
F 6 "JST Sales America Inc." H 3750 5150 50  0001 C CNN "Manufacturer"
F 7 "CONN HEADER VERT 6POS 2.5MM" H 3750 5150 50  0001 C CNN "Description"
	1    3750 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 4950 3250 4950
Text Label 3250 4950 0    50   ~ 0
CS_BODY
Wire Wire Line
	3550 5050 3250 5050
Text Label 3250 5050 0    50   ~ 0
MISO
Wire Wire Line
	3550 5150 3250 5150
Text Label 3250 5150 0    50   ~ 0
SCK
Wire Wire Line
	3550 5250 3250 5250
Text Label 3250 5250 0    50   ~ 0
MOSI
$Comp
L power:GND #PWR0105
U 1 1 5E55F7F1
P 3200 5450
F 0 "#PWR0105" H 3200 5200 50  0001 C CNN
F 1 "GND" V 3205 5322 50  0000 R CNN
F 2 "" H 3200 5450 50  0001 C CNN
F 3 "" H 3200 5450 50  0001 C CNN
	1    3200 5450
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 5450 3550 5450
Wire Wire Line
	3250 5350 3550 5350
$Comp
L tail-rescue:Conn_01x17_feather_left-Feather_connectors J?
U 1 1 5E42AD55
P 2150 2000
AR Path="/5E41758F/5E42AD55" Ref="J?"  Part="1" 
AR Path="/5E42AD55" Ref="J1"  Part="1" 
F 0 "J1" H 2200 2900 50  0000 L CNN
F 1 "Conn_01x17" H 2300 1100 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x17_P2.54mm_Vertical" H 2150 2000 50  0001 C CNN
F 3 "~" H 2150 2000 50  0001 C CNN
F 4 "DNI" H 2100 3050 50  0001 C CNN "DNI"
F 5 "40-0518-10" H 2150 2000 50  0001 C CNN "Manufacturer PN"
F 6 "Aries Electronics" H 2150 2000 50  0001 C CNN "Manufacturer"
F 7 "A460-ND" H 2150 2000 50  0001 C CNN "Digikey PN"
F 8 "" H 2150 2000 50  0001 C CNN "Descrition"
F 9 "CONN SOCKET SIP 40POS GOLD" H 2150 2000 50  0001 C CNN "Description"
	1    2150 2000
	1    0    0    -1  
$EndComp
Text GLabel 3250 5350 0    50   Input ~ 0
+3V3
Wire Wire Line
	1950 1200 1450 1200
Text GLabel 1450 1300 0    50   Input ~ 0
+3V3
Wire Wire Line
	1450 1300 1950 1300
Text Notes 3850 3800 0    50   ~ 0
Wing
Text Notes 3850 4600 0    50   ~ 0
Wing
Text Notes 3850 5400 0    50   ~ 0
Body
$Comp
L Connector_Generic:Conn_01x06 J3
U 1 1 5E4E48A5
P 3750 3550
F 0 "J3" H 3830 3542 50  0000 L CNN
F 1 "Conn_01x06" H 3830 3451 50  0000 L CNN
F 2 "jst:JST_1x6" H 3750 3550 50  0001 C CNN
F 3 "~" H 3750 3550 50  0001 C CNN
F 4 "455-2271-ND" H 3750 3550 50  0001 C CNN "Digikey PN"
F 5 "B6B-XH-A(LF)(SN)" H 3750 3550 50  0001 C CNN "Manufacturer PN"
F 6 "JST Sales America Inc." H 3750 3550 50  0001 C CNN "Manufacturer"
F 7 "CONN HEADER VERT 6POS 2.5MM" H 3750 3550 50  0001 C CNN "Description"
	1    3750 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3350 3250 3350
Wire Wire Line
	3550 3450 3250 3450
Wire Wire Line
	3550 3550 3250 3550
Wire Wire Line
	3550 3650 3250 3650
Text Label 3250 3650 0    50   ~ 0
SDA
Text Label 3250 3550 0    50   ~ 0
SCL
$Comp
L tail-rescue:Conn_01x14_feather_right-Feather_connectors J?
U 1 1 5E42AD5B
P 3300 1800
AR Path="/5E41758F/5E42AD5B" Ref="J?"  Part="1" 
AR Path="/5E42AD5B" Ref="J2"  Part="1" 
F 0 "J2" H 3750 2500 50  0000 C CNN
F 1 "Conn_01x14" H 3400 1000 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H 3300 1800 50  0001 C CNN
F 3 "~" H 3300 1800 50  0001 C CNN
F 4 "40-0518-10" H 3300 1800 50  0001 C CNN "Manufacturer PN"
F 5 "Aries Electronics" H 3300 1800 50  0001 C CNN "Manufacturer"
F 6 "A460-ND" H 3300 1800 50  0001 C CNN "Digikey PN"
F 7 "CONN SOCKET SIP 40POS GOLD" H 3300 1800 50  0001 C CNN "Description"
	1    3300 1800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3500 2300 3950 2300
Text Label 3950 2300 2    50   ~ 0
SCL
Wire Wire Line
	3500 2400 3950 2400
Text Label 3950 2400 2    50   ~ 0
SDA
Text Label 3250 4450 0    50   ~ 0
SDA
Text Label 3250 4350 0    50   ~ 0
SCL
NoConn ~ 3250 3350
NoConn ~ 3250 3450
NoConn ~ 3250 4150
NoConn ~ 3250 4250
NoConn ~ 4350 5450
NoConn ~ 2300 4050
Text Label 1250 1700 0    50   ~ 0
PWM_LEFT_UP
Wire Wire Line
	1950 1900 1250 1900
Text Label 1250 1900 0    50   ~ 0
PWM_LEFT_DN
Wire Wire Line
	1950 2500 1250 2500
Wire Wire Line
	1950 2600 1250 2600
Text Label 1250 2500 0    50   ~ 0
PWM_RIGHT_UP
Text Label 1250 2600 0    50   ~ 0
PWM_RIGHT_DN
NoConn ~ 3500 1700
NoConn ~ 3500 1800
NoConn ~ 3500 1900
NoConn ~ 1950 2100
NoConn ~ 1950 2000
NoConn ~ 1950 1400
NoConn ~ 1950 1600
NoConn ~ 1950 1800
Wire Wire Line
	3500 2100 3950 2100
Text Label 3900 2100 0    50   ~ 0
CS_BODY
$EndSCHEMATC
