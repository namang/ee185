# PCB Design

The hardware for the fractal flyers consists mainly of the head board and tail board. The design files for these boards are located in this directory. We have transitioned to using KiCad for board design, but early (now defunct) boards may use other tools, such as Upverter.

- ether-tail: board for converting RJ45 to microUSB A male plug data lines; used to test signaling
- headboard: headboard
- head-ether: board for converting USB A male plug data lines to RJ45; used to test signaling
- tailboard: tailboard and supporting boards (e.g. signaling board)
- testboard: board for testing LED strips
- pin-reverse: board for reversing FeatherM4 pins 
- sensor-setup: a schematic for sensor/motor testing
# Reference

* [KiCad](https://kicad-pcb.org/)
* [SAM32 Board Files](https://github.com/maholli/SAM32/tree/master/hardware/SAM32_v26)
* [TI WEBENCH Power Designer](https://www.ti.com/design-resources/design-tools-simulation/webench-power-designer.html)
* [Upverter](https://upverter.com/)
