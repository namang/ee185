# Testboards

This directory contains the testboard, which is the board used to test LED strips that have the power/data connector that FLIGHT uses. Early construction of LED strips saw a high failure rate, so we designed this board to easily test them.

  - testboard: original testboard. This board had a fatal flaw in that if an LED strip shorted 5V to data this would put 5V on the Feather M4 pin and fry the Feather. 
  - testboard-v2: current tetboard revision. Include transistor digital pin protection and footprint rework.
  - testboard-seiji: testboard developed by a student, incorporated into v2
  - testboard-joseph: testboard developed by a student, incorporated into v2
