# Fractal Flyer Tailboard PCB

The tailboard is the board on each Fractal Flyer. The headboard is the
board at the other end of the Ethernet cable, connected to many USB ports 
on the controller computer in parallel and 48V power supplies,

The BoM of the tailboard can be found here: <https://www.digikey.com/short/z8m375>

