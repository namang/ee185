#!/bin/sh

cd "$( dirname "$0" )"
mkdir -p build
javac -Xlint:deprecation -source 1.8 -target 1.8 -sourcepath src -classpath "build:lib/*:lib/lx/*:lib/lx/processing-3.5.4/*" src/*/*.java -d build
cd build
jar cf flight.jar model engine
