package model;

public class Model implements Geometry {
  static FlightModel flight;

  public Model(FlyerConfig[] flyerConfig) {
    flight = new FlightModel(flyerConfig);
  }


  public FlightModel getFlightModel() {
    return flight;
  }

}

