#ifndef MICROPY_INCLUDED_ATMEL_SAMD_COMMON_HAL_MOTOR_CONTROL_ACCEL_H
#define MICROPY_INCLUDED_ATMEL_SAMD_COMMON_HAL_MOTOR_CONTROL_ACCEL_H

#include "ports/atmel-samd/common-hal/motor_control/SPI.h"
#include "ports/atmel-samd/common-hal/motor_control/I2C.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>

#define LEFTWING 0
#define CENTER 1
#define RIGHTWING 2

typedef struct {
    float x;
    float y;
    float z;
} acceleration;

typedef uint8_t ACCEL_TYPE;

void init_accels(void);
acceleration get_raw_accel (uint8_t);
void accel_reset(uint8_t);
void init_accels(void);

void test_spi_reading(void);
void test_spi_reset(void);

#endif
