﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoFlightFlap : MonoBehaviour
{
    public GameObject flyer;
    void Start()
    {
    	moveWing("1", 60);
        moveWing("2", -60);
        StartCoroutine(LoopMovement());

    }

    private IEnumerator LoopMovement()
	{
	    while (true)
	    {
	        yield return new WaitForSeconds(2);
	        moveWing("1", -120);
        	moveWing("2", 120);
        	yield return new WaitForSeconds(2);
        	moveWing("1", 120);
        	moveWing("2", -120);
	    }
	}

    void moveWing(string wingId, float angle) {
        GameObject leftWing = flyer.transform.Find("WingLeft").transform.Find("Pivot").gameObject;
        GameObject rightWing = flyer.transform.Find("WingRight").transform.Find("Pivot").gameObject;

        if (wingId == "1") { // Left
            float z = rightWing.transform.eulerAngles.z;
            float x = rightWing.transform.eulerAngles.x;
            float y = rightWing.transform.eulerAngles.y;
            // Debug.Log("Moved right wing to angle " + angle);
            StartCoroutine(Rotate(rightWing, new Vector3(angle - x, 0, 0), 0.5f));
        } else { // Right
            float z = leftWing.transform.eulerAngles.z;
            float x = leftWing.transform.eulerAngles.x;
            float y = leftWing.transform.eulerAngles.y;
            // Debug.Log("Moved left wing to angle " + angle);
            StartCoroutine(Rotate(leftWing, new Vector3(x - angle, 0, 0), 0.5f));
        }
    }

    private IEnumerator Rotate(GameObject objectToRotate, Vector3 angles, float duration ) {
        Quaternion startRotation = objectToRotate.transform.localRotation ;
        Quaternion endRotation = Quaternion.Euler( angles ) * startRotation ;
        for( float t = 0 ; t < duration ; t+= Time.deltaTime )
        {
            objectToRotate.transform.localRotation = Quaternion.Lerp( startRotation, endRotation, t / duration ) ;
            yield return null;
        }
        objectToRotate.transform.localRotation = endRotation;
    }
}
