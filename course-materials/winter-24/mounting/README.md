# Mounting Team: Current Progress

Week 2: currently working on validation in Solidworks and physically building fixture designs.

## Solidworks validation
    * Solidworks install/remote access
    * Learning Solidworks
    * Loading parts/assemblies

## Physical validation
    * Designing mock I-beams, window mounts, and ceiling beams (send proposal/parts list in by Friday to get everything on time)
    * Checking load ratings for stairwell beams
