# Course Materials for Winter 2024

This directory stores the course materials for the Winter 2024
offering of EE285/CS241. Directory structure:
  - electronics: The PCBs and associated signaling
  - mounting: the assemblies that attach Flyers to the stairwell
  - shell: forming and cutting the acrylic body shell
  - signaling: communicating USB differential signaling over an Ethernet twisted pair
  - software: end-to-end software integration


