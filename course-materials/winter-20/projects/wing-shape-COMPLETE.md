# Project: Wing Shape and LEDs

*Instructor Lead: Charles Gadeken*

**Due: Tuesday, January 14th, 2020**

The goals and deliverables for this project are:
  1. Decide on the wing shape,
  2. Decide which edges of the wings will have LEDs,
  3. Determine how the LEDs will be attached to the wings, and
  4. Order parts to test and verify these decisions.

## Wing Shape

There are three wing shapes in the cad/ subdirectory of the course
git repository. Cut and etch one instance of each set. 

## Wing LEDs

Explore attaching LEDs to one or more edges of each wing shape. 
For each configuration, try different color patterns and timing,
observing how the wings look in darkess and as part of the larger shape.
Based on these observations, decide which wing shape we should use.

## Wing attachment

Explore ways to attach the LEDs to the wings. Should we use flexible
LED strips or rigid printed circuit boards? Last quarter we reached
rough consensus that edges of the wings should be covered in some way,
so they do not appear as bright lines of light (lightsabers). Decide
how we will accomplish this. Order the parts to implement your design.
Fully implementing the design and testing it will be a follow-on project.
