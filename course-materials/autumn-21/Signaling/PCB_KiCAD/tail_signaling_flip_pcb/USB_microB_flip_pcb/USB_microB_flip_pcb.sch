EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L tailboard_signalling_pcb-rescue:U-G-M5WS-W-01-tailboard_signalling_pcb J2
U 1 1 6198F129
P 6100 3700
F 0 "J2" H 6330 3746 50  0000 L CNN
F 1 "U-G-M5WS-W-01-tailboard_signalling_pcb" H 6330 3655 50  0000 L CNN
F 2 "tailboard_signalling_pcb:HRO_U-G-M5WS-W-01" H 6100 3700 50  0001 L BNN
F 3 "https://lcsc.com/product-detail/USB-Connectors_Korean-Hroparts-Elec-U-G-M5WS-W-01_C283546.html" H 6100 3700 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 6100 3700 50  0001 L BNN "STANDARD"
F 5 "2017/12/26" H 6100 3700 50  0001 L BNN "PARTREV"
F 6 "HRO Electronics Co., Ltd." H 6100 3700 50  0001 L BNN "MANUFACTURER"
F 7 "1.8mm" H 6100 3700 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
	1    6100 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 61991CEC
P 4950 3800
F 0 "J1" H 5030 3792 50  0000 L CNN
F 1 "Conn_01x04" H 5030 3701 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x04_P2.00mm_Vertical" H 4950 3800 50  0001 C CNN
F 3 "~" H 4950 3800 50  0001 C CNN
	1    4950 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 61996DCF
P 4500 4000
F 0 "#PWR02" H 4500 3750 50  0001 C CNN
F 1 "GND" H 4505 3827 50  0000 C CNN
F 2 "" H 4500 4000 50  0001 C CNN
F 3 "" H 4500 4000 50  0001 C CNN
	1    4500 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 61997F5D
P 5450 4000
F 0 "#PWR05" H 5450 3750 50  0001 C CNN
F 1 "GND" H 5455 3827 50  0000 C CNN
F 2 "" H 5450 4000 50  0001 C CNN
F 3 "" H 5450 4000 50  0001 C CNN
	1    5450 4000
	1    0    0    -1  
$EndComp
Text GLabel 4750 3900 0    50   Input ~ 0
D+
Text GLabel 4750 3800 0    50   Input ~ 0
D-
Text GLabel 5700 3700 0    50   Input ~ 0
D+
Text GLabel 5700 3600 0    50   Input ~ 0
D-
Wire Wire Line
	4500 4000 4750 4000
$Comp
L power:+5V #PWR01
U 1 1 6199C742
P 4500 3700
F 0 "#PWR01" H 4500 3550 50  0001 C CNN
F 1 "+5V" H 4515 3873 50  0000 C CNN
F 2 "" H 4500 3700 50  0001 C CNN
F 3 "" H 4500 3700 50  0001 C CNN
	1    4500 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3700 4750 3700
$Comp
L power:+5V #PWR06
U 1 1 6199DC99
P 5550 3500
F 0 "#PWR06" H 5550 3350 50  0001 C CNN
F 1 "+5V" H 5565 3673 50  0000 C CNN
F 2 "" H 5550 3500 50  0001 C CNN
F 3 "" H 5550 3500 50  0001 C CNN
	1    5550 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3500 5700 3500
Wire Wire Line
	5700 3400 5700 3150
Wire Wire Line
	5700 3150 5450 3150
Wire Wire Line
	5450 3150 5450 3900
Wire Wire Line
	5450 4000 5700 4000
Connection ~ 5450 4000
Wire Wire Line
	5450 3900 5700 3900
Connection ~ 5450 3900
Wire Wire Line
	5450 3900 5450 4000
NoConn ~ 5700 3800
$Comp
L power:+5V #PWR03
U 1 1 619A02DD
P 4900 4750
F 0 "#PWR03" H 4900 4600 50  0001 C CNN
F 1 "+5V" H 4915 4923 50  0000 C CNN
F 2 "" H 4900 4750 50  0001 C CNN
F 3 "" H 4900 4750 50  0001 C CNN
	1    4900 4750
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 619A2C0B
P 4900 4750
F 0 "#FLG01" H 4900 4825 50  0001 C CNN
F 1 "PWR_FLAG" H 4900 4923 50  0000 C CNN
F 2 "" H 4900 4750 50  0001 C CNN
F 3 "~" H 4900 4750 50  0001 C CNN
	1    4900 4750
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 619A3DC5
P 5350 4750
F 0 "#FLG02" H 5350 4825 50  0001 C CNN
F 1 "PWR_FLAG" H 5350 4923 50  0000 C CNN
F 2 "" H 5350 4750 50  0001 C CNN
F 3 "~" H 5350 4750 50  0001 C CNN
	1    5350 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 619A5328
P 5350 4750
F 0 "#PWR04" H 5350 4500 50  0001 C CNN
F 1 "GND" H 5355 4577 50  0000 C CNN
F 2 "" H 5350 4750 50  0001 C CNN
F 3 "" H 5350 4750 50  0001 C CNN
	1    5350 4750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
