# Assignment 1: Making a Plan 

*Written by Philip Levis*

**Due: Tuesday, September 28th, 2021 at 10:00 PM**

This quarter, you're each going to work in a group to
complete an important piece of FLIGHT. When embarking on
an engineering project such as this, you want to plan.
What's going to take a lot of time? What can you work
on in parallel? How are you going to complete your task
on time without pulling lots of all-nighters?

But even before you get to making a plan, you want to
talk about *how* you will work together and come up
with shared goals. You don't want to just be a group.
You want to be *team*.

## Goals
When you're done with this assignment, you should have

- grown from a group into a team, with shared goals
and expectations;

- read the core design materials for a FLIGHT and
a Fractal Flyer, understanding both your part and
how the whole system comes together;

- have written a plan and timeline for your project
over the next 7 weeks.

## 1 Form a Team
*cribbed from Erin MacDonald*

The most important parts of a successful team are:

  1. Finding a time when everyone meets, as a group, each week, for at least two hours.
  2. Having similar expectations for the course and the amount of time you will spend on it. Each student should expect to spend about nine hours outside of class per week on FLIGHT.
  3. Having a mix of skills. 

We've tried to help with #3. The first part of this assignment is about #1 and #2.

### Find a Time to Meet

Compare your schedules to find a time block of at least two hours when you all
can meet. Commit to meeting at this time each week and working together. If 
something comes up and someone can't make it, be sure to schedule another time
for that week.

### Write a Charter

A team charter is a document of your own design. It should be "artfully" designed, 
expressing some interests and passions of your team. Things to discuss:
  1. What are your goals for the class? Please have each member discuss individually.
  2. Talk about some triumphs and challenges on past team assignments.
  4. Take this quiz: https://whatkindofdesigner.com/. Discuss whether or not you agree with the classification. It will give you a starting point for discussing your interests in the design process.

Include in the charter:

  1. What is your team mascot? 
  2. How will the team celebrate triumphs?
  3. How will the team make important decisions? 
  4. How will the team resolve conflicts and discuss problems?
  5. Leadership: What does "leadership" mean to your team? Who is the team leader to start with? How often or under what circumstances will you switch leaders? (Recommended that you switch at least 1 time and no more than 2 times. I think it is better to choose a date in advance, that way there are no hard feelings when you switch.)
  6. Who is the person that hits "submit" on your reports and milestones? The one who crosses the "t"s and dots the "i"s? This is a big job, possibly more work than the leader, so plan accordingly. You can assign different people to be in charge of different reports and milestones, but it helps to have a clear person in charge for each one.
  7. What are the skills of the team members? What special skill does everyone bring to the team?
  8. When will the team meet as a group each week? Please be precise.
  9. What will be the procedure for missing or being late to this meeting? How much advance notice must be given and using what method? (We recommend that last-minute text messaging is not used to inform team members that a member will be late or miss a meeting.) 

## 2 Review Existing Materials

The course repository has a lot of materials on FLIGHT and
Fractal Flyers. Understanding your project, and how it fits
into the larger whole, is an important first step to being
able to complete the project. If you don't know what you
have to do, then it's very hard to do it! This is especially
important in engineering a complex system, where there are
many dependencies and constraints from all of the different
parts. 

Go to the [course repository](https://code.stanford.edu/plevis/ee185).
If you are comfortable with the `git` tool, you can clone
it and browse the files locally. If you're not familiar with `git`,
don't worry about it: just browse the files through the web interface.

Poke around the repository a bit to see what's in there. You'll
find that not all of the project materials are there. In particular,
the CAD for a Fractal Flyer is in an online CAD program called
OnShape. There's a link to the CAD model in the README for
the project, which is rendered as a web page at the top level
of the repository when viewing it through the web interface.
You should be able to view the CAD but not modify it -- if you
want to play with it, make a copy for yourself and do so. As 
you get further along in projects, we'll open up write permissions
to the CAD. As you poke around the repository, find out the answers
to these questions:
 1. Where do you find materials/documents for this quarter's class?
 1. Where do you find Python scripts that run on a Fractal Flyer?
 2. Where do you find architectural drawings of the Packard building?
 3. Why did we choose to choose acrylic pre-treated with dichroic film
 as the wing material?

Once you have looked around a bit, read the following documents:
  - The Flyer Construction Manual. This is a detailed, step-by-step
  instruction manual on how to assemble a Fractal Flyer. It is written
  for the old motors; once we are 100% sure we're using the current
  motor design we'll update it for them. Reading this will give you
  a good sense of the physicality of a Flyer and all of the dependencies
  in it.
  - The FLIGHT design document. This gives an overview of the entire system,
  talking about issues in each component, how they've arisen, and how they
  are solved. 

## 3 Planning (the next seven weeks of) Your Future

Now that you've talked, have shared your expectations, 
have thought about how you will work as a team, and understand
your project, it's time to do your first joint task: making a 
plan and schedule.

This plan is your roadmap for the next seven weeks. You're going
to think about exactly what you have to achieve and how you're
going to do it. You want to take a large, multifaceted project
and break it up into smaller, more predictable, manageable tasks.

This roadmap is not sarosanct: we expect it will change and
adjust as we talk you through it and as issues come up. But when
things do change, having thought a plan through will let you
more easily understand the implications and how you want to adapt
in response.

Recall that the last 2 weeks of the quarter will be building
a Fractal Flyer and getting it working. You're therefore making
a plan for weeks 2-8, at which point you should have completed 
your project with sufficient quantity that the class can build 
5 Fractal Flyers.

Your plan should:

  - Complete the tasks laid out in your one-page project description.
  - Have a week-by-week statement of expected progress. Some
  tasks will take more than 1 week. In this case, try breaking it
  up into multiple tasks. Come up with smaller steps that you can
  test and check. 
  - Clearly delineate tasks that occur in parallel. For example,
  if half of your group is going to research connectors and half
  is going to make a mould, show these as two separate tasks 
  occurring in parallel.
  - Have at least 2 major milestones. These should be significant
  pieces of work which, when completed, represent a triumph or
  large accomplishment.
  - Include written reports that explain designs and important
  design decisions. It is better to write a small number of
  smaller reports on separate topics than trying to write one
  big report at the end. These reports serve two audiences:
  the current class, so you can explain and justify your
  decisions, and future contributors to FLIGHT, so they can
  learn what you know and carry that knowledge forward.
  - State which aspects of your plan and estimates have the 
  greatest and least certainty. Uncertainty can be because 
  you don't know, or because there are factors outside your
  control. 
  - Include risk mitigation: if something goes wrong, what is
  your plan B? What will you fall back to? It could be that the 
  fallback is less attractive, costs more, or has other limitations.
  Thinking through other approaches is a key part of coming up
  with a good approach: otherwise, it's too easy to tunnel.
  - Write down any expected dependencies on other groups. On what
  will you need to coordinate, and when?
  - Are there big unknowns, tools you need, or expertise your are missing? If 
  you could have an advisor or resource of some kind (e.g., an
  expert on the thermal and physical propeties of acrylic,
  or a vector network analyzer), tell us what it is so we
  can help.

Your plan should be 2-3 pages. You can use a spreadsheet to 
show timelines, or figures, or tables. whatever works best for you.

If you have questions or are uncertain about certain aspects of
the Flyer, do not hesitate to reach out. You can email all of us
at the staff mailing list ee185-aut2122-staff@lists.stanford.edu.

We will share and discuss the plans in class on Wednesday.

## 4 Handing in

Please send an email to the staff list with subject "Assignment 1"
which has your your charter and plan  Please send both as attachments
(PDF if possible). Do not send links to Google docs, or Drive entries,
or Dropbox files. If you spoke with people outside the class in coming
up with your plan (e.g., on certain technical topics), please mention
them in your report. 

